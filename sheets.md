# Build A Sheets enabled function a(8)

a(8) has built support for Google Sheets that enables any function to be called right from your spreadhseet.

## Initializing A Function

To initialize a function with a Sheets transformer, simply type:

`a8 init --runtime node --sheets <your function name>`

This will add the following code to `func.js`:

``` Javascript
fdk.sheets(function(result) {
    const sheets = []
    sheets.push(["Header 1", "Header 2"])
    sheets.push(["Data 1", "data 2"])
    return {sheets}
})
```

As you can see, the sheets format is pretty simple. The outer array represents the sheet itself. Each element in the sheet array represents a row. Each row is an array that contains a value for the cell at that position in the row.

## calling your function in Sheets

Calling your function from Sheets is simple. Here's the function format:

`=a8("function.name", "param 1 name", "param 1 value", "param 2 name", "param 2 value")`

The params and values will be transformed into the following JSON and passed to your function:

``` JSON
{
    "param 1 name": "param 1 value",
    "param 2 name": "param 2 value"
}
```

## Adding Sheets To An Existing Function

If you want to add a Sheets transformer to an existing function all you have to do is copy and paste the code above into `func.js`

## How Does This Work?

It's simple. First, your function's `fdk.handle` method will be called. You do all the normal things that make your function special and output the result in any way you'd like. Once you return from handle, `fdk.sheets` will be called enabling you to transform the result of your function to Sheets format. The result that's returned is subsequently returned to Sheets and inserted into the document.