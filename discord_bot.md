# Build A Discord Bot Using autom(🤖)

a(8) has built in bot support for Discord, making it easy for you and others to interact with your functions!

## Initializing A Function

To initialize a function with a Discord transformer, simply type:

`a8 init --runtime node --discord <your function name>`

This will add the following code to `func.js`:

``` Javascript
fdk.discord(function(result){
    return {
        "content" : result.message + " in discord form!",
        "embed" : {
          "description" : "[Click here](https://leovoel.github.io/embed-visualizer/) for an example of what you can do in Discord responses."
        }
    }
})
```

## Running Your Function In Discord

There are two formats you can use to run functions in Discord:

`a8! <your function name> param1:foo, param2:bar`

AND

`a8! <your funciton> long string, of text`

You'll see the difference in the JSON that's passed as input to your function. In the former form you'll get:

``` JSON
{
    "param1": "foo",
    "param1": "bar"
}
```

And the latter:

``` JSON
{
    "_inputs": ["long string", "of text"]
}
```

It is your function's responsibility to make sense of the input it gets.

## Adding Discord To An Existing Function

If you want to add a Discord transformer to an existing function all you have to do is copy and paste the code above into `func.js`

## How Does This Work?

It's simple. First, your function's `fdk.handle` method will be called. You do all the normal things that make your function special and output the result in any way you'd like. Once you return from handle, `fdk.discord` will be called enabling you to transform the result of your function to Discord format. The result that's returned is subsequently returned to Discord.

## Discord Message Formatting

The output of the discord transformer `fdk.discord` should return the typical Discord JSON format:

``` json
{
    "content" : "",
    "embed" : {
      ...
    }
}
```

To see all the ways one can style Discord messages see [here](https://leovoel.github.io/embed-visualizer/).

## Meta Fields

a8 passes the fields it gets from Discord through to your function's `fdk.handle` method. You can access them via the `_discord` field in the input JSON. This can be useful for finding such things as the name of the user making the call or the channel the call was made in.  

Full JSON that is deliverd to user is below, to get information on these fields look in the [official Discord message object documention](https://discordapp.com/developers/docs/resources/channel#message-object)  
  
 ``` json
{  
   "_discord":{  
      "id": string,
      "channel_id": string,
      "content": string,
      "timestamp": string,
      "edited_timestamp": string,
      "mention_roles":[  
        string,
      ],
      "tts": bool,
      "mention_everyone": bool,
      "author":{  
         "id": string,
         "email": string,
         "username": string,
         "avatar": string,
         "locale": string,
         "discriminator": string,
         "token": string,
         "verified": bool,
         "mfa_enabled": bool,
         "bot": bool
      },
      "attachments":[  
      {
        "id" : string,
        "url" : string,
        "proxy_url" : string,
        "filename" : string,
        "width" : int,
        "height" : int,
        "size" : int
        },
      ],
      "embeds":[  
        DiscordMessageEmbed (see Message Formatting)
      ],
      "mentions":[  
        {  
           "id": string,
           "email": string,
           "username": string,
           "avatar": string,
           "locale": string,
           "discriminator": string,
           "token": string,
           "verified": bool,
           "mfa_enabled": bool,
           "bot": bool
        }
      ],
      "reactions":null,
      "type": int,
      "webhook_id":  string
   },
   "_transformer":"discord"
}
 ```