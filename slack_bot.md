# Build A Slack Bot Using autom(🤖)

a(8) has built in bot support for Slack, making it easy for you and others to interact with your functions!

## Initializing A Function

To initialize a function with a Slack transformer, simply type:

`a8 init --runtime node --slack <your function name>`

This will add the following code to `func.js`:

``` Javascript
fdk.slack(function(result){
    return {
        "response_type": "in_channel",
        "blocks" : [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "hello world in slack form! \n <https://api.slack.com/tools/block-kit-builder| Click here> to see what you can do with Slack formatting!"
                }
            }
        ]
    }
})
```

## Running Your Function In Slack

There are two formats you can use to run functions in Slack:

`/a8 <your function name> param1:foo, param2:bar`

AND

`/a8 <your funciton> long string, of text`

You'll see the difference in the JSON that's passed as input to your function. In the former form you'll get:

``` JSON
{
    "param1": "foo",
    "param1": "bar"
}
```

And the latter:

``` JSON
{
    "_inputs": ["long string", "of text"]
}
```

It is your function's responsibility to make sense of the input it gets.

## Adding Slack To An Existing Function

If you want to add a Slack transformer to an existing function all you have to do is copy and paste the code above into `func.js`

## How Does This Work?

It's simple. First, your function's `fdk.handle` method will be called. You do all the normal things that make your function special and output the result in any way you'd like. Once you return from handle, `fdk.slack` will be called enabling you to transform the result of your function to Slack format. The result that's returned is subsequently returned to Slack.

## Slack Message Formatting

The output of the slack transformer `fdk.slack` should return the typical Slack JSON format:

``` json
{
    "response_type": "",
    "blocks" : [
        ...
    ]
}
```

To see all the ways one can style the Slack messages see [here](https://api.slack.com/tools/block-kit-builder). Design using the block kit format and simply copy and paste the text as the value of the `block` key in your Slack JSON output.