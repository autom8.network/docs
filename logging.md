# Log Capture In a(8)

At this point, you can only view logs for functions you deploy. Viewing logs for chains of functions is currently under construction 🚧

## 1. Choose A Logging Service

The first step to viewing logs is to choose an online logging service, like [Papertrail.io](https://www.papertrail.io/), that supports shipping logs via syslog.

## 2. Setup A Logging Destination

This may vary per logging service, so we'll list the steps for [Papertrail.io](https://www.papertrail.io/):

1. Go to `Settings` (top right hand corner), click on `Log Destinations`, and click `Create a Log Destination`.
1. In the create dialog, under TCP unselect `TLS` and under both TCP and UDP select `Plain Text`
1. Click `Create`
1. You’ll see the address of your log destination displayed at the top of the page looking something like `logs7.papertrailapp.com:<PORT>`. Copy this value to your clipboard for use in a minute.

## 3. Tell a(8) Where To Send Your Logs

To add the logger you just configured to your functions, simply type this at the command line:

`a8 create logger [copied syslog destination]`

The next time you do `a8 invoke your.fuction` you should see logs appear in your logging service of choice shortly after you run functions. Keep in mind that there may be some delay between running the function and logs appearing.

Need help? [Slack us](https://a8-slack.herokuapp.com)!